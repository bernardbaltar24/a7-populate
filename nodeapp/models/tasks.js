//Declare Dependencies

const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const validator = require("validator");

//Define your schema
//description, teamId-String, isCompleted-boolean
const taskSchema = new Schema(
	{
		description: {
			type:String,
			required: true,
			maxlength: 100
		},

		memberId: {
			type: mongoose.Schema.Types.ObjectId,
			required: true,
			ref: "Member"
		},

		isCompleted: {
			type: Boolean,
			default: false
		}
	},
	{
		timestamps: true
	}
	);

//Export your model

module.exports = mongoose.model("Task", taskSchema);