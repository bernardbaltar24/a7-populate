//Declare dependencies and model
const Team = require("../models/teams");
const express = require("express");
const router = express.Router(); //to handle routing
const auth = require("../middleware/auth");


//Create Routes/Endpoints
//1) CREATE
router.post("/", auth, async (req, res) =>{
	// return res.send(req.body);
	const team = new Team(req.body);
	//save to DB
	// team.save()
	// .then(() => {res.send(team)})
	// .catch((e) => {res.status(400).send(e)}) //BAD REQUEST (http response status codes)
	try{
		await team.save();
		res.send(team);
	}catch(e){
		res.status(400).send(e)
	}
});

//2)GET ALL
router.get("/", auth, async(req, res)=>{
	// return res.send("get all teams");
	// Team.find().then((teams) => { return res.status(200).send(teams)})
	// .catch((e) => { return res.status(500).send(e)})

	try{
		const teams = await Team.find();
		res.status(200).send(teams)
	}catch(e){
		return res.status(404).send(e)
	}
})

//3)GET ONE
router.get("/:id", auth, async(req, res) =>{
	// return res.send("get a team");
	// console.log(req.params.id)
	const _id = req.params.id;

	//Mongose Models Query
	// Team.findById(_id).then((team) => {if(!team){
		//NOT Found
	// 	return res.status(404).send(e)
	// } return res.send(team)

	// })
	// .catch((e) => {return res.status(500).send(e)})

	const match = {};
	const sort = {}

	if(req.query.position){
		match.position = req.query.position}

	if(req.query.sortBy){
		const parts = req.query.sortBy.split(":");
		sort[parts[0]] = parts[1] === "desc" ? -1 : 1 //sort desc = -1 if not 1 for asc
	}

	try{
		const team = await Team.findById(_id);
		if(!team){
		return res.status(404).send("WALANG MAHANAP")}; //if wala mahanap return this
		
		await req.team.populate({
			path: "members",
			match,
			options: {
				limit: parseInt(req.query.limit),
				skip: parseInt(req.query.skip),
				sort //sort: sort
			}
		}).execPopulate();
		res.status(200).send(team.members) //if wala return this
		}catch(e){
		res.status(500).send(e)};
})

//GET TEAMS MEMBER
// router.get("/teams/:id", async(req, res) =>{
// 	const _id = req.params.id;

// 	try{
// 		const team = await Team.populate("members").execPopulate();
// 		if(!team){
// 		return res.status(404).send("WALANG MAHANAP")}; //if wala mahanap return this
// 		res.send(team) //if wala return this
// 		}catch(e){
// 		res.status(500).send(e)};

// })

//4)UPDATE ONE
router.patch("/:id", auth, async (req, res) =>{
	// return res.send("update a team");
	const _id = req.params.id

	// Team.findByIdAndUpdate(_id, req.body, {new:true}).then((team) => {
	// 	if(!team){return res.status(404).send(e)}
	// 	return res.send(team)
	// })
	// .catch((e) => {return res.status(500).send(e)})
	try{
		const team = await Team.findByIdAndUpdate(_id, req.body, { new:true });
		if(!team){
		return res.status(404).send("WALANG MAHANAP")};
		res.send(team)
		} catch(e){
		return res.status(500).send(e)
		}
})

//5)DELETE ONE
router.delete("/:id", auth, async (req, res)=> {
	// return res.send("delete a team");

	const _id = req.params.id;

	// Team.findByIdAndDelete(_id).then((team) => {if(!team){return res.status(404).send(e)}
	// 	return res.send(team)
	// })
	// .catch((e) => {return res.status(500).send(e)})
	try{
		const team = await Team.findByIdAndDelete(_id);
		if(!team){
			return res.status(404).send("Team doesn't exist")
		}	res.send(team)
	}catch(e){
		res.status(500).send(e.message)
	}
})
	
module.exports = router;