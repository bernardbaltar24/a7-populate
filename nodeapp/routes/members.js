//Declare dependencies and model
const Member = require("../models/members");
const express = require("express");
const router = express.Router(); //to handle routing
const bcrypt = require('bcryptjs');
const auth = require("../middleware/auth");

//-------MEMBERS
// 1) CREATE MEMBER 
router.post("/", auth, async (req, res) =>{
	const member = new Member(req.body);
	// member.save().then(() => {res.send(member)})
	// .catch((e) => {res.status(400).send(e)})
	try{
		await member.save();
		res.send(member); //or you can use res.status(201).send(member)
	}catch(e){
		res.status(400).send(e)
	}
})
//2 GET ALL MEMBER

router.get("/", auth, async (req, res)=>{
	// Member.find().then((members) => { return res.status(200).send(members)})
	// .catch((e) => { return res.status(500).send(e)})
	const match = {};
	const sort = {}
	if(req.query.position){
		match.position = req.query.position}

	if(req.query.sortBy){
		const parts = req.query.sortBy.split(":");
		sort[parts[0]] = parts[1] === "desc" ? -1 : 1 //sort desc = -1 if not 1 for asc
	}

	try {
		await req.member.populate({
			path: "members",
			match,
			options: {
				limit: parseInt(req.query.limit),
				skip: parseInt(req.query.skip),
				sort //sort: sort
			}
		}).execPopulate();
		res.status(200).send(req.member)
	} catch(e){
		return res.status(404).send(e)
	}
}) 

//7) GET LOGIN USER'S PROFILE

router.get("/me", auth, async (req, res) => {
	console.log("test");
	// res.send(req.member)
	// res.send(req.token)
	res.send(req.member)
})


//3)GET ONE MEMBER
router.get("/:id", auth, async (req, res) =>{
	const _id = req.params.id;
	// Member.findById(_id).then((member) => {if(!member){
	// 	return res.status(404).send(e)
	// } return res.send(member)
	// })
	// .catch((e) => {return res.status(500).send(e)})
	try{
		const member = await Member.findById(_id);
		if(!member){
		return res.status(404).send("WALANG MAHANAP NA member")};
		res.send(member)
	}catch(e){
		res.status(500).send(e)
	}
})
//4)UPDATE ONE MEMBER (update own profile)
router.patch("/me", auth, async (req, res) =>{
	// const _id = req.params.id
	const updates = Object.keys(req.body);

	const allowedUpdates = ["firstName", "lastName", "position", "password", "teamId"];

	const isValidUpdate = updates.every(update => allowedUpdates.includes(update))

	if(!isValidUpdate){
		return res.status(400).send({error: "Invalid update"})
	}
	// Member.findByIdAndUpdate(_id, req.body, {new:true}).then((member) => {
	// 	if(!member){return res.status(404).send(e)}
	// 	return res.send(member)
	// })
	// .catch((e) => {return res.status(500).send(e)})
	try{
		// const member = await Member.findByIdAndUpdate(_id, req.body, { new:true }); 

		// const member = await Member.findById(_id);
		updates.map(update => (req.member[update] = req.body[update]));

		// if(!member){
		// return res.status(404).send("WALANG MAHANAP")};

		await req.member.save();

		res.send(req.member);
	} catch(e){
		return res.status(500).send(e)
		}
})
//5)DELETE ONE MEMBER
router.delete("/:id", auth, async (req, res)=> {
	const _id = req.params.id;
	// Member.findByIdAndDelete(_id).then((member) => {if(!member){return res.status(404).send(e)}
	// 	return res.send(member)
	// })
	// .catch((e) => {return res.status(500).send(e)})
	try{
		const member = await Member.findByIdAndDelete(_id);
		if(!member){
			return res.status(404).send("Member doesn't exist or whatever")
		}	res.send(member)
	}catch(e){
		res.status(500).send(e.message)
	}
})

//6) LOGIN
router.post("/login", async(req, res) => {
	try{
		//submit email and password
		// const member = await Member.findByCredentials(req.body.email, req.body.password);
	const member = await Member.findOne({$or: [{email: req.body.email}, {username: req.body.email}]})



		if(!member){
			return res.send({"message":"Invalid login credentials!"})} 

		const isMatch = await bcrypt.compare(req.body.password, member.password);

		if(!isMatch){
			return res.send({"message":"Invalid login credentials!"}) //for now
		}
		//generate token

		const token = await member.generateAuthToken();

		res.send({member, token});

	} catch(e){
		res.status(500).send(e);
	}
})



//8) LOGOUT

//9) LOGOUT ALL

router.post('/logout', auth, async (req, res) => {
  try {
  	req.member.tokens.splice(0);
  	await req.member.save();
  	res.send("Log out ka na sa lahat!")

    /*
      CLUES:
      Study the result in your console. 
      Create a contant variable called remainingTokens. Its value should be a method that filters 
      a member's token so that it doesn't include the req's token.
      Assign the result of remainingTokens to the member's token.
      Await saving of changes made to member.
      Send back the member
    */
  } catch (e) {
    //Send back status 500 and the error
  res.status(500).send(e);
  }
});


module.exports = router;