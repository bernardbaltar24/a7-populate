//declare dependencies

const express = require("express");
const app = express();
const mongoose = require("mongoose");

//connect to DB
mongoose.connect("mongodb://localhost:27017/mern_tracker2", {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useCreateIndex: true
});

mongoose.connection.once("open", () => {
	console.log("Now connected to local MongoDB");
});

//Apply Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));

//Declare Model
const Team = require("./models/teams");
const Member = require("./models/members");
const Task = require("./models/tasks")

//Create Routes/Endpoints

//Trasferred Routes

//Declare the resources
const teamsRoute = require("./routes/teams");
app.use("/teams", teamsRoute);

const tasksRoute = require("./routes/tasks");
app.use("/tasks", tasksRoute);

const membersRoute = require("./routes/members");
app.use("/members", membersRoute);


//initialize the server

app.listen(5000, () => {
	console.log("Now listening to port 5000");
});